# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.5)

file(MAKE_DIRECTORY
  "C:/ESP32/esp-idf/components/bootloader/subproject"
  "D:/Titoma/Firmware/FW7/CodeESP32/program/build/bootloader"
  "D:/Titoma/Firmware/FW7/CodeESP32/program/build/bootloader-prefix"
  "D:/Titoma/Firmware/FW7/CodeESP32/program/build/bootloader-prefix/tmp"
  "D:/Titoma/Firmware/FW7/CodeESP32/program/build/bootloader-prefix/src/bootloader-stamp"
  "D:/Titoma/Firmware/FW7/CodeESP32/program/build/bootloader-prefix/src"
  "D:/Titoma/Firmware/FW7/CodeESP32/program/build/bootloader-prefix/src/bootloader-stamp"
)

set(configSubDirs )
foreach(subDir IN LISTS configSubDirs)
    file(MAKE_DIRECTORY "D:/Titoma/Firmware/FW7/CodeESP32/program/build/bootloader-prefix/src/bootloader-stamp/${subDir}")
endforeach()
