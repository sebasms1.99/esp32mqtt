/* MQTT Mutual Authentication Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>

#include "esp_wifi.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "protocol_examples_common.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"

#include "lwip/err.h"
#include "lwip/sys.h"

#include "esp_spi_flash.h"
#include "driver/uart.h"
#include "driver/gpio.h"

#include "nvs.h"
#include "lwip/err.h"
#include "lwip/sys.h"
#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"

#include "esp_log.h"
#include "mqtt_client.h"

#define EX_UART_NUM                         UART_NUM_1 //UART_NUM_1 //UART_NUM_0
#define UART1_TX                            GPIO_NUM_16
#define UART1_RX                            GPIO_NUM_17

#define BUF_SIZE                            (1024)
#define RD_BUF_SIZE                         (BUF_SIZE)

#define EXAMPLE_ESP_WIFI_SSID               "Salazar"
#define EXAMPLE_ESP_WIFI_PASS               "34659007"

#define AWS_HOST                            "a1rv95j8gy7wcr-ats.iot.us-west-2.amazonaws.com"
#define SUBSCRIBE_TOPIC                     CLIENT_ID"/requests"
#define PUBLISH_TOPIC                       "$aws/things/"CLIENT_ID"/shadow/update"
#define PUBLISH_DATA                        "{"                                                 \
                                                "\"state\": {"                                  \
                                                    "\"reported\": {"                           \
                                                    "\"project\": \"unal_\","                   \
                                                    "\"serial_number\": \"xxxxxxxxxx\","        \
                                                    "\"report\": {"                             \
                                                        "\"room_temp\": \"25\","                \
                                                        "\"door_status\": \"open\","            \
                                                        "\"heater_status\": \"on\","            \
                                                        "\"fan_status\": \"on\","               \
                                                        "\"fan_speed\": \"200\","               \
                                                        "\"alarm_status\": \"off\","            \
                                                        "\"error_code\": \"0x00000000\""        \
                                                    "}"                                         \
                                                    "}"                                         \
                                                "}"                                             \
                                            "}"

#define PUBLISH_DATA_PACKET()                "{"                                                \
                                                "\"state\": {"                                  \
                                                    "\"reported\": {"                           \
                                                    "\"project\": \"unal_\","                   \
                                                    "\"serial_number\": \"%s\","                \
                                                    "\"report\": {"                             \
                                                        "\"room_temp\": \"%s\","                \
                                                        "\"door_status\": \"%s\","              \
                                                        "\"heater_status\": \"%s\","            \
                                                        "\"fan_status\": \"%s\","               \
                                                        "\"fan_speed\": \"%s\","                \
                                                        "\"alarm_status\": \"%s\","             \
                                                        "\"error_code\": \"%s\""                \
                                                    "}"                                         \
                                                    "}"                                         \
                                                "}"                                             \
                                            "}"

#define PUBLISH_FW_PACKET()                 "{"                                                 \
                                                "\"state\": {"                                  \
                                                    "\"reported\": {"                           \
                                                    "\"project\": \"unal_\","                   \
                                                    "\"serial_number\": \"%s\","                \
                                                    "\"fw\": \"%s\""                            \
                                                    "}"                                         \
                                                "}"                                             \
                                            "}"


#define MQTT_PORT                           8883
#define EXAMPLE_ESP_MAXIMUM_RETRY           5
#define WIFI_CONNECTED_BIT                  BIT0
#define WIFI_FAIL_BIT                       BIT1

static const char *TAG = "AWS_TEST";
static QueueHandle_t uart0_queue;
static EventGroupHandle_t s_wifi_event_group;
static int s_retry_num = 0;

esp_mqtt_client_handle_t client;

#define CLIENT_SN                                   "1063815038"
#define CLIENT_ID                                   "unal_"CLIENT_SN
extern const uint8_t aws_root_ca_pem_start[]        asm("_binary_unal_root_CA_crt_start");
extern const uint8_t aws_root_ca_pem_end[]          asm("_binary_unal_root_CA_crt_end");

extern const uint8_t client_cert_pem_start[]        asm("_binary_unal_1063815038_certificate_pem_crt_start");
extern const uint8_t client_cert_pem_end[]          asm("_binary_unal_1063815038_certificate_pem_crt_end");

extern const uint8_t client_key_pem_start[]         asm("_binary_unal_1063815038_private_pem_key_start");
extern const uint8_t client_key_pem_end[]           asm("_binary_unal_1063815038_private_pem_key_end");


// message packet  to operate
char TEMP[8] = {0}; // Temp
char DOOR[6] = {0};  // door status
char HEATER[4] = {0};  // Heater status 
char FAN[6] = {0};  // fan status 
char SPED[3] = {0};  // fan speed 

char open_msg[] = "open";
char close_msg[] = "close";
char on_msg[] = "on";
char off_msg[] = "off";
char rqst_msg[15] = "report_request";

static void uart_event_task(void *pvParameters)
{
    uart_event_t event;
    uint8_t *dtmp = (uint8_t *) malloc(RD_BUF_SIZE);

    for (;;)
    {
        // Waiting for UART event.
        if (xQueueReceive(uart0_queue, (void *)&event, (portTickType)portMAX_DELAY))
        {
            bzero(dtmp, RD_BUF_SIZE);
            // ESP_LOGI(TAG, "uart[%d] event:", EX_UART_NUM);

            switch (event.type)
            {

                case UART_DATA:
                    // ESP_LOGI(TAG, "[UART DATA]: %d", event.size);
                    uart_read_bytes(EX_UART_NUM, dtmp, event.size, portMAX_DELAY);
                    //uart_write_bytes(EX_UART_NUM, (const char *) dtmp, event.size);//Echo

                    char pub_buf[300];
                    memset(pub_buf, 0, sizeof(pub_buf));

                    char rpm_val_str[21];
                    memmove(rpm_val_str, dtmp, event.size);

                    if(rpm_val_str[0] == '*' && rpm_val_str[1] == 'T'){ // TEMPERATURE
                        for(int i = 0;i<4;i++){
                            if(rpm_val_str[0])
                            TEMP[i]=rpm_val_str[i];
                        }
                    char* start = strchr(rpm_val_str, 'T') + 1; 
                    char* stop = strchr(rpm_val_str, '#');            
                    strncpy(TEMP, start, stop - start);  
                    }                   
                        

                    if(rpm_val_str[8] == '*' && rpm_val_str[9] == 'D'){  // DOOR
                        if(rpm_val_str[10] == '1'){
                            for(int i = 0; i<sizeof(DOOR);i++){
                                DOOR[i] = open_msg[i];
                            }
                        }
                        
                        if(rpm_val_str[10] == '0'){
                            
                            for(int i = 0; i<sizeof(DOOR);i++){
                                DOOR[i] = close_msg[i];
                            }
                        }
                    }
                    
                    if(rpm_val_str[18] == '*' && rpm_val_str[19] == 'H'){ // HEATER

                        if(rpm_val_str[20] == '1'){
                            for(int i = 0; i<sizeof(HEATER);i++){
                                HEATER[i] = on_msg[i];
                            }
                        }
                        
                        if(rpm_val_str[18] == '0'){
                            
                            for(int i = 0; i<sizeof(HEATER);i++){
                                HEATER[i] = off_msg[i];
                            }
                        }
                    }

                    if(rpm_val_str[12] == '*' && rpm_val_str[13] == 'F'){ // fAN status and speed

                        if(rpm_val_str[14] != '0' || rpm_val_str[15] != '0' || rpm_val_str[16] != '0' ){
                            for(int i = 0; i<sizeof(FAN);i++){
                                FAN[i] = on_msg[i];
                            }

                            for(int i = 0; i<3;i++){
                                SPED[i] = rpm_val_str[i+16];
                            }

                        }else{
                            for(int i = 0; i<sizeof(FAN);i++){
                                FAN[i] = off_msg[i];
                            }

                            for(int i = 0; i<3;i++){
                                SPED[i] = '0';
                            }
                            
                        }

                    } 
                    //Send JSON
                    ESP_LOGI(TAG, "%s", rpm_val_str);
                    ESP_LOGI(TAG, "%s", "SENDING THE DATA TO THE SERVER");
                    sprintf(pub_buf, PUBLISH_DATA_PACKET(),
                    "1063815038",
                    TEMP, 
                    DOOR, 
                    HEATER,  
                    FAN, 
                    SPED, 
                    "disabled",
                    "0x12300000");
                    
                    esp_mqtt_client_publish(client, PUBLISH_TOPIC, pub_buf, 0, 0, 0);

                    break;

                case UART_FIFO_OVF:
                    ESP_LOGI(TAG, "hw fifo overflow");
                    uart_flush_input(EX_UART_NUM);
                    xQueueReset(uart0_queue);
                    break;

                case UART_BUFFER_FULL:
                    ESP_LOGI(TAG, "ring buffer full");
                    uart_flush_input(EX_UART_NUM);
                    xQueueReset(uart0_queue);
                    break;

                case UART_PARITY_ERR:
                    ESP_LOGI(TAG, "uart parity error");
                    break;

                case UART_FRAME_ERR:
                    ESP_LOGI(TAG, "uart frame error");
                    break;

                default:
                    ESP_LOGI(TAG, "uart event type: %d", event.type);
                    break;
            }
        }
    }

    free(dtmp);
    dtmp = NULL;
    vTaskDelete(NULL);
}

static void wifi_event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START)
    {
        esp_wifi_connect();
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
    {
        if (s_retry_num < EXAMPLE_ESP_MAXIMUM_RETRY)
        {
            esp_wifi_connect();
            s_retry_num++;
            ESP_LOGI(TAG, "retry to connect to the AP");
        }
        else
        {
            xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
        }
        ESP_LOGI(TAG,"connect to the AP fail");
    }
    else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
    {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(TAG, "got ip:%s",
                 ip4addr_ntoa(&event->ip_info.ip));
        s_retry_num = 0;
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    }
}

void wifi_init_sta(void)
{
    s_wifi_event_group = xEventGroupCreate();

    tcpip_adapter_init();

    ESP_ERROR_CHECK(esp_event_loop_create_default());

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &wifi_event_handler, NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &wifi_event_handler, NULL));

    wifi_config_t wifi_config = {
        .sta = {
            .ssid = EXAMPLE_ESP_WIFI_SSID,
            .password = EXAMPLE_ESP_WIFI_PASS
        },
    };

    if (strlen((char *)wifi_config.sta.password)) {
        wifi_config.sta.threshold.authmode = WIFI_AUTH_WPA2_PSK;
    }

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());

    ESP_LOGI(TAG, "wifi_init_sta finished.");

    /* Waiting until either the connection is established (WIFI_CONNECTED_BIT) or connection failed for the maximum
     * number of re-tries (WIFI_FAIL_BIT). The bits are set by wifi_event_handler() (see above) */
    EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
            WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
            pdFALSE,
            pdFALSE,
            portMAX_DELAY);

    /* xEventGroupWaitBits() returns the bits before the call returned, hence we can test which event actually
     * happened. */
    if (bits & WIFI_CONNECTED_BIT)
    {
        ESP_LOGI(TAG, "connected to ap SSID:%s password:%s", EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS);
    }
    else if (bits & WIFI_FAIL_BIT)
    {
        ESP_LOGI(TAG, "Failed to connect to SSID:%s, password:%s", EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS);
    }
    else
    {
        ESP_LOGE(TAG, "UNEXPECTED EVENT");
    }

    ESP_ERROR_CHECK(esp_event_handler_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP, &wifi_event_handler));
    ESP_ERROR_CHECK(esp_event_handler_unregister(WIFI_EVENT, ESP_EVENT_ANY_ID, &wifi_event_handler));
    vEventGroupDelete(s_wifi_event_group);
}

static esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event)
{
    client = event->client;
    int msg_id;

    switch (event->event_id)
    {
        case MQTT_EVENT_CONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");

            msg_id = esp_mqtt_client_subscribe(client, SUBSCRIBE_TOPIC, 0);
            ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);
            break;

        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
            break;

        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id); 
   
            char pub_buf[300];
            memset(pub_buf, 0, sizeof(pub_buf));

            sprintf(pub_buf, PUBLISH_DATA_PACKET(),
                CLIENT_SN,
                "30.3",
                "open",
                "off",
                "on",
                "366",
                "off",
                "0x12300000");

            ESP_LOGI(TAG, "%s", pub_buf);

            msg_id = esp_mqtt_client_publish(client, PUBLISH_TOPIC, pub_buf, 0, 0, 0);
            // msg_id = esp_mqtt_client_publish(client, PUBLISH_TOPIC, PUBLISH_DATA, 0, 0, 0);
            ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);
            break;

        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;

        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
 
        case MQTT_EVENT_DATA:
            ESP_LOGI(TAG, "MQTT_DATA_RECEIVED");
            ESP_LOGI(TAG, "TOPIC=%.*s", event->topic_len, event->topic);
            ESP_LOGI(TAG, "DATA=%.*s", event->data_len, event->data);
            #define lenght_slect_carac 28
            char recive_mqtt_string[50];                    
            memset(recive_mqtt_string, 0, sizeof(recive_mqtt_string));                    
            memmove(recive_mqtt_string, event->data, lenght_slect_carac);

            char pub_buf_mqtt[300];
            memset(pub_buf_mqtt, 0, sizeof(pub_buf_mqtt));  
        

            switch (recive_mqtt_string[lenght_slect_carac-1])
            {
               
                case 'r': //Update_Request
                    
                    uart_write_bytes(EX_UART_NUM, (const char *) "*GL#", 4);
    
                    break;

                case 'f': //FW_Request
                    ESP_LOGI(TAG, "%s", "SENDING THE FW VERISON");
                    
                    sprintf(pub_buf_mqtt, PUBLISH_FW_PACKET(),
                    "1063815038",            
                    "V1.0.20230601");        

                    esp_mqtt_client_publish(client, PUBLISH_TOPIC, pub_buf_mqtt, 0, 0, 0);

                    break;
                default:
                    break;
            }       

            break;
            

        case MQTT_EVENT_ERROR:
            ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
            break;

        default:
            ESP_LOGI(TAG, "Other event id:%d", event->event_id);
            break;
    }

    return ESP_OK;
}

static void mqtt_app_start(void)
{
    const esp_mqtt_client_config_t mqtt_cfg = {
        .port               = MQTT_PORT,
        .host               = AWS_HOST,
        .client_id          = CLIENT_ID,
        .event_handle       = mqtt_event_handler,
        .cert_pem           = (const char *) aws_root_ca_pem_start,
        .client_cert_pem    = (const char *) client_cert_pem_start,
        .client_key_pem     = (const char *) client_key_pem_start,
        .protocol_ver       = MQTT_PROTOCOL_V_3_1_1,
        .transport          = MQTT_TRANSPORT_OVER_SSL,
    };

    ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
    esp_mqtt_client_handle_t client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_start(client);
}

void app_main(void)
{
#if 1 /* using UART */
    uart_config_t uart_config = {
        .baud_rate = 115200,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
    };

    uart_param_config(EX_UART_NUM, &uart_config);
    uart_driver_install(EX_UART_NUM, BUF_SIZE * 2, BUF_SIZE * 2, 100, &uart0_queue, 0);
    uart_set_pin(EX_UART_NUM, UART1_RX, UART1_TX, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);

    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    ESP_LOGI(TAG, "This is ESP32 chip with %d CPU cores, WiFi, ", chip_info.cores);
    ESP_LOGI(TAG, "silicon revision %d, ", chip_info.revision);
    ESP_LOGI(TAG, "%dMB %s flash", spi_flash_get_chip_size() / (1024 * 1024), (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");

    xTaskCreate(uart_event_task, "uart_event_task", 2 * 1024, NULL, 12, NULL);
#endif

    ESP_ERROR_CHECK(nvs_flash_init());

#if 1 /* using WI-Fi */
    ESP_ERROR_CHECK(esp_netif_init());
    // ESP_ERROR_CHECK(esp_event_loop_create_default());

    ESP_LOGI(TAG, "ESP_WIFI_MODE_STA");
    wifi_init_sta();
#endif

#if 1 /* using MQTT */
    ESP_LOGI(TAG, "[APP] Startup..");
    ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "[APP] IDF version: %s", esp_get_idf_version());

    mqtt_app_start();
#endif
}
